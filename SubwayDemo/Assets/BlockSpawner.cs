using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockSpawner : MonoBehaviour
{
    public Transform[] spawnPointsArray;
    public float timeBetweenWaves = 1f;
    private float timeToSpawn = 2f;
    public GameObject blockPrefab;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (Time.time >= timeToSpawn)
        {
            SpawnBlocks();
            timeToSpawn = Time.time + timeBetweenWaves;
        }

    }

    void SpawnBlocks()
    {
        int randomIndex = Random.Range(0, spawnPointsArray.Length);

        for (int i = 0; i < spawnPointsArray.Length; i++)
        {
            if (randomIndex != i)
            {
                Instantiate(blockPrefab, spawnPointsArray[i].position, Quaternion.identity);
            }
        }
    }

}
