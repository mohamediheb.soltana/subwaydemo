using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Vector2 initPos;
    private Vector2 endPos;
    public Rigidbody rb;
    public float sideForce = 2000f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
       /* if (Input.GetKey("d"))
        {
            rb.AddForce(sideForce, 0, 0);
        }
        //if (endPos.x < (Screen.width / 2))
        if (Input.GetKey("q"))
        {
            rb.AddForce(-sideForce, 0, 0);

        }*/
         if (Input.touchCount > 0)
         {
             if (Input.GetTouch(0).phase == TouchPhase.Began)
             {
                 initPos = Input.GetTouch(0).position;
             }

             if (Input.GetTouch(0).phase == TouchPhase.Moved)
             {
                 Debug.Log(Input.GetTouch(0).position);
             }
             if (Input.GetTouch(0).phase == TouchPhase.Ended)
             {
                 endPos = Input.GetTouch(0).position;

                 if (endPos.x > (Screen.width/2))
                 {
                     rb.AddForce(sideForce, 0, 0 );
                 }
                 if (endPos.x < (Screen.width / 2))
                 {
                     rb.AddForce(-sideForce, 0, 0);
                 }
             }
         }
    }
}
